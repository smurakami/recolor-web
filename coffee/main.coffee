# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

main = null
class Main
  constructor: ->
    main = @
    new Header
    new About
    new Works
    new Creators
  scrollTo: (selector) ->
    header = $ '#header'
    $("html,body").animate
      scrollTop: $(selector).offset().top - header.height()
  isSmartphone: -> $(window).width() <= 640
  disableScroll: ->
    $('html').addClass 'disable_scroll'
  enableScroll: ->
    $('html').removeClass 'disable_scroll'

class Header
  constructor: ->
    nav = $ '#header .nav'
    nav.find('.about').click => main.scrollTo('#about')
    nav.find('.works').click => main.scrollTo('#works')
    nav.find('.creators').click => main.scrollTo('#creators')
    nav.find('.contact').click => main.scrollTo('#contact')

class About
  constructor: ->
    @initCSS()
    @initBackground()
    $(window).resize =>
      @initCSS()
  initCSS: ->
    if main.isSmartphone()
      $('#about').css 'height', $(window).height()
      top = ($(window).height() - $('#header').height() - ($('#about .logo').height() + $('#about .copy').height())) / 2
      $('#about .front').css 'top', $('#header').height() + top

    $('#about .background').css 'height', $('#about').height()
    $('#about .background .photo').css 'height', $('#about').height()
    $('#about .background .photo_cover').css 'height', $('#about').height()
    $('#about .background .cover').css 'height', $('#about').height()

  initBackground: ->
    @photo = $('#about .background .photo')
    @photo_cover = $('#about .background .photo_cover')
    @image_urls = $('#works li .image img').map ->
      $(this).attr 'src'
    .filter ->
      this.length > 0

    @photo_index = 0
    @photo.css 'background-image', "url(#{@image_urls[@photo_index]})"
    setInterval =>
      @photo_index++
      if @photo_index >= @image_urls.length
        @photo_index = 0
      @changeBackground(@photo_index)
    , 2000
  changeBackground: (index) ->
    image_url = @image_urls[index]
    @photo_cover.css 'background-image', "url(#{@image_urls[@photo_index]})"
    @photo_cover.css 'opacity', 0
    @photo_cover.removeClass 'hidden'
    @photo_cover.animate
      opacity: 1
    , null, null, =>
      @photo.css 'background-image', "url(#{@image_urls[@photo_index]})"
      @photo_cover.addClass 'hidden'


class Works
  constructor: ->
    @initImages()
    works = $ '#works'
    self = @
    works.find('li').click (x) ->
      self.setupPop($(this))
      self.showPop()
    $('#works_pop .background').click => @hidePop()
    $('#works_pop .back_button').click => @hidePop()
  initImages: ->
    $('#works li').each (i) ->
      src = $(this).find('.data .image img').attr 'src'
      $(this).find('.photo').css 'background-image', "url(#{src})"

  setupPop: (li) ->
    pop = $ '#works_pop .contents'

    console.log li

    title = li.find('.data .title').text()
    pop.find('.title').text title
    html = li.find('.data .description').html()
    pop.find('.description').html html

    # if li.find('.data .movie').size() > 0
    #   pop.find('.movie').removeClass 'hidden'
    #   pop.find('.image').addClass 'hidden'
    #   url = li.find('.data .movie').text()
    #   console.log url
    #   console.log url.match(/v=(\w+)/)
    #   console.log url.match(/v=(\w+)/)[1]

    #   id = url.match(/v=(\w+)/)[1]
    #   pop.find('.movie iframe').attr 'src', "https://www.youtube.com/embed/#{id}?rel=0&amp;controls=0&amp;showinfo=0"
    # else
    #   pop.find('.movie').addClass 'hidden'
    #   pop.find('.image').removeClass 'hidden'
    #   src = li.find('.data .image img').attr 'src'
    #   pop.find('.image').css 'background-image', "url(#{src})"
  showPop: ->
    $('#works_pop').removeClass 'hidden'
    $('#works_pop').css 'opacity', 0
    $('#works_pop .contents').scrollTop(0)
    $('#works_pop').animate
      opacity: 1
    main.disableScroll()
  hidePop: ->
    $('#works_pop').css 'opacity', 1
    $('#works_pop').animate
      opacity: 0
    , null, null, ->
      $('#works_pop').addClass 'hidden'
    main.enableScroll()

class Creators
  constructor: ->
    creators = $ '#creators'
    self = @
    creators.find('li').click (x) ->
      self.setupPop($(this))
      self.showPop()
    $('#creators_pop .background').click =>
      @hidePop()
    $('#creators_pop .back_button').click =>
      @hidePop()
  setupPop: (li) ->
    src = li.find('.photo img').attr 'src'
    name = li.find('.name').text()
    title = li.find('.data .title').text()
    text = li.find('.data .text').text()
    policy = li.find('.data .policy').text()
    pop = $ '#creators_pop .contents'
    pop.find('.image img').attr 'src', src
    pop.find('.name').text name
    pop.find('.title').text title
    pop.find('.text').text text
    pop.find('.policy').text policy
  showPop: (index) ->
    $('#creators_pop').removeClass 'hidden'
    $('#creators_pop').css 'opacity', 0
    $('#creators_pop .contents').scrollTop(0)
    $('#creators_pop').animate
      opacity: 1
    main.disableScroll()
  hidePop: ->
    $('#creators_pop').css 'opacity', 1
    $('#creators_pop').animate
      opacity: 0
    , null, null, ->
      $('#creators_pop').addClass 'hidden'
    main.enableScroll()

$ ->
  new Main
